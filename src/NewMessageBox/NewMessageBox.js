import React from "react";
import "./NewMessageBox .css";
import sendMessage from "../AuxFunction/sendMessage";
class NewMessageBox extends React.Component {
  state = {
    newMessageInput: ""
  };
  newMessageInput = event => {
    this.setState({ newMessageInput: event.target.value });
  };
  clearState = () => {
    this.setState({ newMessageInput: "" });
  };
  keyPress = event => {

    if (event.key == "Enter")
      sendMessage(this.state.newMessageInput, this.clearState);
  };
  render() {
    return (
      <div className="new-message">
        <input
          className="new-message-input"
          onChange={this.newMessageInput}
          value={this.state.newMessageInput}
          onKeyPress={this.keyPress}
        ></input>
        <button
          onClick={() => {
            sendMessage(this.state.newMessageInput, this.clearState);
          }}
          className="send-button"
        >
          Send
        </button>
      </div>
    );
  }
}

export default NewMessageBox;
