import firebase from "firebase";
const firebaseConfig = {
  apiKey: "AIzaSyCGqhYVscHYN3G-W12X3huMJWW6u7XQkgE",
  authDomain: "messagingapp-f44bc.firebaseapp.com",
  databaseURL: "https://messagingapp-f44bc.firebaseio.com",
  projectId: "messagingapp-f44bc",
  storageBucket: "messagingapp-f44bc.appspot.com",
  messagingSenderId: "650537402698",
  appId: "1:650537402698:web:cdbdc53d9af1c5b171c1b3",
  measurementId: "G-NLQJM3FGGM"
};
firebase.initializeApp(firebaseConfig);
export default firebase.database;
