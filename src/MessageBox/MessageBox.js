import React from "react";
import "./MessageBox.css";
const MessageBox = props => {
  const style =
    props.userId === localStorage.getItem("userId")
      ? { marginLeft: "auto", background: "aliceblue" }
      : {};
  return (
    <div className="message-box" style={style}>
      <div className="message-head">UserID:{props.userId}</div>
      <hr />
      <div className="message-body">{props.message}</div>
    </div>
  );
};

export default MessageBox;
