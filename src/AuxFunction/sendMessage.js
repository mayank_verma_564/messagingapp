import database from "../firebase";
const sendMessage = (message, clear, event) => {
  if (message) {
    let databaseref = database().ref("chat");
    let userId = localStorage.getItem("userId");
    if (userId) {
      databaseref.push({
        userId,
        message
      });
      clear();
    } else {
      localStorage.setItem("userId", new Date().valueOf());
    }
  }
};
export default sendMessage;
