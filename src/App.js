import React from "react";
import PreviousChat from "./PreviousChat";
import "./App.css";
import NewMessageBox from "./NewMessageBox/NewMessageBox";
function App() {
  return (
    <div className="wrapper">
      <div className="container">
        <PreviousChat />
        <NewMessageBox />
      </div>
    </div>
  );
}

export default App;
