import React from "react";
import { connect } from "react-redux";
import {getPreviousChat} from './redux/action'
import database from "./firebase";
import MessageBox from "./MessageBox/MessageBox";
class PreviousChat extends React.Component {
    componentDidMount()
    {
        this.props.dispatch(getPreviousChat(database))
    }
  render() {
      let previousChat=this.props.previousChat;
    let previousChatList=Object.keys(previousChat).map((value,index)=>
    {
        return <MessageBox userId={previousChat[value].userId} message={previousChat[value].message} key={index}/>  
    })
    return (
        <div style={{height:'95%',overflow:'auto'}}>
        {previousChatList}
        </div>
    );
  }
}
const mapStateToProps=state=>(
    {
      previousChat:state.previousChat  
    }
);
export default connect(mapStateToProps)(PreviousChat);
