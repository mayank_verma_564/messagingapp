import { createStore, applyMiddleware } from "redux";
import { savePreviousChat } from "./action";
import thunk from "redux-thunk";
const initialState = {
  previousChat: []
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case savePreviousChat:
      return {
        previousChat: action.payload
      };
    default:
      return state;
  }
};
const store = createStore(reducer, applyMiddleware(thunk));
export default store;
