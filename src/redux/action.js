export const savePreviousChat = "SAVE_PREVIOUS_CHAT";
export const getPreviousChat = database => {
  return dispatch => {
    let databaseref = database().ref("chat");
    databaseref.on("value", function(data) {
      dispatch({ type: savePreviousChat, payload: data.val() });
    });
  };
};
